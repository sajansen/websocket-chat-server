import json
from datetime import datetime


class DatetimeEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, datetime):
            return str(obj)
        return json.JSONEncoder.default(self, obj)


class WsMessageEncoder(json.JSONEncoder):
    def default(self, obj):
        from objects import WsMessage
        if isinstance(obj, WsMessage):
            return obj.json()
        return json.JSONEncoder.default(self, obj)


class MainJsonEncoder(json.JSONEncoder):
    def default(self, o):
        return WsMessageEncoder.default(WsMessageEncoder(), o)
