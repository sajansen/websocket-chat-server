CREATE TABLE IF NOT EXISTS messages
(
    id        integer PRIMARY KEY,
    from_name text NOT NULL,
    to_name   text,
    timestamp text,
    message   text,
    type      text NOT NULL,
    is_removed integer DEFAULT 0
);