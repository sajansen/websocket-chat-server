CREATE TABLE IF NOT EXISTS clients
(
    id                integer PRIMARY KEY,
    name              text NOT NULL,
    created_on        text,
    last_connected    text,
    last_disconnected text
);