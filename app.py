import logging

import db
import ws

logger = logging.getLogger(__name__)
logging.basicConfig(
        format='[%(levelname)s] %(asctime)s %(name)s {%(module)s} | %(message)s',
        level=logging.DEBUG)


def main():
    logger.info("Starting application")
    try:
        db.connect()
        ws.start()
    finally:
        ws.close()
        db.close()


if __name__ == "__main__":
    main()
