import logging
import sqlite3
from typing import Optional, List

import config
from objects import WsMessage, WsClient

logger = logging.getLogger(__name__)

connection: Optional[sqlite3.Connection] = None


def connect():
    global connection
    try:
        connection = sqlite3.connect(config.DATABASE_FILE)
        connection.row_factory = sqlite3.Row
        logger.info("Connected to database")
    except sqlite3.Error as e:
        logger.error("Error while connecting to the database")
        logger.exception(e)
        close()

    init()


def close():
    if connection:
        connection.close()
    logger.info("Database connection closed")


def init():
    logger.info("Creating database tables")
    create_table_from_file("sql/messages.sql")
    create_table_from_file("sql/clients.sql")


def create_table_from_file(file: str):
    with open(file, "r") as file:
        create_table(file.read())


def create_table(sql):
    logger.debug("query: {}".format(sql))
    try:
        cursor: sqlite3.Cursor = connection.cursor()
        cursor.execute(sql)
        connection.commit()
    except sqlite3.Error as e:
        logger.error(e)


def insert_message(message: WsMessage) -> int:
    logger.info("Inserting message: {}".format(message))

    query = message.db_insert_query()
    logger.debug("query: {} with {}".format(query[0], query[1]))
    try:
        cursor: sqlite3.Cursor = connection.cursor()
        cursor.execute(query[0], query[1])
        connection.commit()
        logger.info("Inserted new message")
    except sqlite3.Error as e:
        logger.error(e)
        return -1

    message.id = cursor.lastrowid
    return cursor.lastrowid


def get_all_messages_from_and_to_name(name: str) -> List[WsMessage]:
    logger.info("Finding messaged from and to name: {}".format(name))

    query = WsMessage.db_find_from_and_to_name_query(name)
    logger.debug("query: {} with {}".format(query[0], query[1]))
    try:
        cursor: sqlite3.Cursor = connection.cursor()
        cursor.execute(query[0], query[1])
        data = cursor.fetchall()
    except sqlite3.Error as e:
        logger.error(e)
        return []

    if data is None:
        return []

    messages = list(map(lambda o: WsMessage.from_dict(dict(o)), data))
    return messages


def insert_client(client: WsClient) -> int:
    logger.info("Inserting client: {}".format(client))

    query = client.db_insert_query()
    logger.debug("query: {} with {}".format(query[0], query[1]))
    try:
        cursor: sqlite3.Cursor = connection.cursor()
        cursor.execute(query[0], query[1])
        connection.commit()
    except sqlite3.Error as e:
        logger.error(e)
        return -1

    client.id = cursor.lastrowid
    logger.info("Inserted new client {}".format(client))
    return cursor.lastrowid


def update_client(client: WsClient) -> Optional[WsClient]:
    logger.info("Updating client: {}".format(client))

    query = client.db_update_query()
    logger.debug("query: {} with {}".format(query[0], query[1]))
    try:
        cursor: sqlite3.Cursor = connection.cursor()
        cursor.execute(query[0], query[1])
        connection.commit()
    except sqlite3.Error as e:
        logger.error(e)
        return None

    logger.info("Updated client {}".format(client))
    return client


def get_client_by_name(name: str) -> Optional[WsClient]:
    logger.info("Finding client by name: {}".format(name))

    query = WsClient.db_find_by_name_query(name)
    logger.debug("query: {} with {}".format(query[0], query[1]))
    try:
        cursor: sqlite3.Cursor = connection.cursor()
        cursor.execute(query[0], query[1])
        data = cursor.fetchone()
    except sqlite3.Error as e:
        logger.error(e)
        return None

    if data is None:
        return None

    logger.info(dict(data))
    return WsClient.from_dict(dict(data))


def delete_client(client: WsClient):
    logger.info("Deleting client: {}".format(client))

    query = client.db_delete_query()
    logger.debug("query: {} with {}".format(query[0], query[1]))
    try:
        cursor: sqlite3.Cursor = connection.cursor()
        cursor.execute(query[0], query[1])
        connection.commit()
    except sqlite3.Error as e:
        logger.error(e)
        return None

    logger.info("Deleted client {}".format(client))
