import abc
import json
from enum import Enum
from typing import Any, Tuple

import websockets

from encoders import MainJsonEncoder
from utils import get_timestamp


class DbObject(abc.ABC):

    @abc.abstractmethod
    def db_insert_query(self) -> Tuple[str, Tuple[any]]:
        pass

    @abc.abstractmethod
    def db_update_query(self) -> Tuple[str, Tuple[any]]:
        pass

    @abc.abstractmethod
    def db_delete_query(self) -> Tuple[str, Tuple[any]]:
        pass


class WsMessageType(Enum):
    UNKNOWN = "UNKNOWN"
    CONNECTION_INIT = "CONNECTION_INIT"
    CONNECTION_CLOSE = "CONNECTION_CLOSE"
    CHAT_MESSAGE = "CHAT_MESSAGE"
    BOT_MESSAGE = "BOT_MESSAGE"
    ACTION = "ACTION"
    ACTION_RESULT = "ACTION_RESULT"


class WsActionType(Enum):
    GET_ALL_ONLINE_USERS = "GET_ALL_ONLINE_USERS"
    GET_ALL_CHAT_MESSAGES_FROM_AND_TO_USER = "GET_ALL_CHAT_MESSAGES_FROM_AND_TO_USER"


class WsMessage(DbObject):
    def __init__(self, message_type: WsMessageType,
                 message: str,
                 from_name: str = None,
                 to_name: str = None,
                 id: int = None,
                 timestamp: str = None):
        self.id = id
        self.from_name = "server" if from_name is None else from_name
        self.to_name = to_name
        self.message = message
        self.timestamp = get_timestamp() if timestamp is None else timestamp
        self.type: WsMessageType = message_type
        self.is_removed = False

    def json(self):
        return json.dumps({
            "id": self.id,
            "from": self.from_name,
            "to": self.to_name,
            "message": self.message,
            "timestamp": self.timestamp,
            "type": self.type.value,
            "is_removed": self.is_removed
        }, cls=MainJsonEncoder)

    @staticmethod
    def from_dict(v: dict):
        message_type = WsMessageType[v.get("type", "UNKNOWN")]
        return WsMessage(message_type,
                         v.get("message", ""),
                         v.get("from", v.get("from_name", "unknown")),
                         v.get("to", v.get("to_name", "unknown")),
                         id=v.get("id", None),
                         timestamp=v.get("timestamp", None))

    @staticmethod
    def from_json(v: str):
        return WsMessage.from_dict(json.loads(v))

    def __repr__(self):
        return "[WsMessage: id={}; from_name={}; to_name={}; timestamp={}; type={}; message={}, is_removed={}]" \
            .format(self.id,
                    self.from_name,
                    self.to_name,
                    self.timestamp,
                    self.type,
                    self.message,
                    self.is_removed)

    def db_insert_query(self) -> Tuple[str, Tuple[any]]:
        return '''INSERT INTO messages(from_name, to_name, "timestamp", message, "type", "is_removed") 
        VALUES (?,?,?,?,?,?)''', \
               (self.from_name, self.to_name, self.timestamp, self.message, self.type.value, self.is_removed)

    def db_update_query(self) -> Tuple[str, Tuple[any]]:
        return '''UPDATE messages SET from_name=?, to_name=?, timestamp=?, message=?, type=?, is_removed=? WHERE id=?''', \
               (self.from_name, self.to_name, self.timestamp, self.message, self.type.value, self.is_removed, self.id)

    def db_delete_query(self) -> Tuple[str, Tuple[any]]:
        return '''DELETE FROM messages WHERE id=?''', \
               (self.id,)

    @staticmethod
    def db_find_from_and_to_name_query(name: str) -> Tuple[str, Tuple[any]]:
        return '''SELECT * FROM messages WHERE from_name=? OR to_name=?''', (name,name,)


class WsClient(DbObject):

    def __init__(self, name: str,
                 websocket: websockets.WebSocketServerProtocol = None,
                 created_on: str = None,
                 last_connected: str = None,
                 last_disconnected: str = None,
                 id: int = None):
        self.id = id
        self.name = name
        self.ws = websocket
        self.created_on = get_timestamp() if created_on is None else created_on
        self.last_connected = get_timestamp() if last_connected is None else last_connected
        self.last_disconnected = None if last_disconnected is None else last_disconnected

    @staticmethod
    def from_dict(v: dict):
        return WsClient(name=v.get("name", ""),
                        created_on=v.get("created_on", None),
                        last_connected=v.get("last_connected", None),
                        last_disconnected=v.get("last_disconnected", None),
                        id=v.get("id", None)
                        )

    def db_insert_query(self) -> Tuple[str, Tuple[any]]:
        return '''INSERT INTO clients(name, created_on, last_connected, last_disconnected) VALUES (?,?,?,?)''', \
               (self.name, self.created_on, self.last_connected, self.last_disconnected)

    def db_update_query(self) -> Tuple[str, Tuple[any]]:
        return '''UPDATE clients SET name=?, created_on=?, last_connected=?, last_disconnected=? WHERE id=?''', \
               (self.name, self.created_on, self.last_connected, self.last_disconnected, self.id)

    def db_delete_query(self) -> Tuple[str, Tuple[any]]:
        return '''DELETE FROM clients WHERE id=?''', \
               (self.id,)

    @staticmethod
    def db_find_by_name_query(name: str) -> Tuple[str, Tuple[any]]:
        return '''SELECT * FROM clients WHERE name=?''', (name,)

    def __repr__(self):
        return "[WsClient: id={}; name={}; created_on={}; last_connected={}; last_disconnected={}]" \
            .format(self.id,
                    self.name,
                    self.created_on,
                    self.last_connected,
                    self.last_disconnected)


class WsActionResult:
    def __init__(self, action_name: str,
                 is_success: bool,
                 message: str = "",
                 data: Any = None,
                 exception: str = "") -> None:
        self.action_name = action_name
        self.is_success = is_success
        self.message = message
        self.data = data
        self.exception = exception

    def json(self) -> str:
        return json.dumps({
            "action_name": self.action_name,
            "is_success": self.is_success,
            "message": self.message,
            "data": self.data,
            "exception": self.exception,
        }, cls=MainJsonEncoder)

    def __repr__(self) -> str:
        return "[WsActionResult: action_name={}; is_success={}; message={}; data={}; exception={}]".format(
                self.action_name,
                self.is_success,
                self.message,
                self.data,
                self.exception)
