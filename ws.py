import asyncio
import logging
from typing import Set

import config
import db
from objects import *

logger = logging.getLogger(__name__)

clients: Set[WsClient] = set()


async def register(client: WsClient):
    logger.info("Registering client {}".format(client))

    client.last_connected = get_timestamp()

    clients.add(client)
    db.insert_client(client)


async def unregister(client: WsClient):
    logger.info("Unregistering client {}".format(client))

    client.last_disconnected = get_timestamp()

    clients.remove(client)

    db.update_client(client)


async def handle_action_request(client: WsClient, message: WsMessage) -> WsActionResult:
    if message.type != WsMessageType.ACTION:
        return WsActionResult(action_name=message.message, is_success=False, message="Message isn't ACTION type")

    try:
        action: WsActionType = WsActionType[message.message]
    except Exception as e:
        return WsActionResult(action_name=message.message, is_success=False, message="Message isn't valid ActionType",
                              exception=str(e))

    if action == WsActionType.GET_ALL_ONLINE_USERS:
        usernames = list(map(lambda c: c.name, clients))
        return WsActionResult(action_name=action.value, is_success=True, data={"usernames": usernames})

    elif action == WsActionType.GET_ALL_CHAT_MESSAGES_FROM_AND_TO_USER:
        messages = db.get_all_messages_from_and_to_name(client.name)
        messages = list(filter(lambda m:
                               m.type == WsMessageType.BOT_MESSAGE or m.type == WsMessageType.CHAT_MESSAGE, messages))
        return WsActionResult(action_name=action.value, is_success=True, data={"messages": messages})


async def listen(client: WsClient):
    async for ws_data in client.ws:
        logger.debug("Received data from client '{}': {}".format(client.name, ws_data))
        message = WsMessage.from_json(ws_data)
        db.insert_message(message)

        if message.type == WsMessageType.CONNECTION_INIT:
            client.name = message.from_name
            await send_message(client, WsMessage(WsMessageType.CHAT_MESSAGE,
                                                 "Hello {}!".format(client.name),
                                                 to_name=client.name))

            await sync_new_client_with_db(client)

        elif message.type == WsMessageType.CHAT_MESSAGE:
            await handle_chat_message(client, message)

        elif message.type == WsMessageType.ACTION:
            action_result = await handle_action_request(client, message)

            logger.info(action_result)
            logger.info(action_result.json())
            await send_message(client, WsMessage(WsMessageType.ACTION_RESULT,
                                                 action_result.json(),
                                                 to_name=client.name))


async def sync_new_client_with_db(client):
    existing_client = db.get_client_by_name(client.name)

    if existing_client is not None:
        db.delete_client(client)

        client.id = existing_client.id
        client.created_on = existing_client.created_on
        client.last_disconnected = existing_client.last_disconnected

    db.update_client(client)


async def handle_chat_message(client: WsClient, message: WsMessage):
    if message.to_name == "server":
        reply_message = WsMessage(WsMessageType.CHAT_MESSAGE,
                                  f"I'm listening: {message.message}!",
                                  to_name=client.name)
        await send_message(client, reply_message)
    else:
        for c in clients:
            if not (message.to_name == "all" or message.to_name == c.name) or c == client:
                continue

            logger.info("Passing message to {}".format(c.name))
            await c.ws.send(message.json())


async def send_message(client: WsClient, message: WsMessage):
    db.insert_message(message)
    await client.ws.send(message.json())


async def handle_connection(websocket: websockets.WebSocketServerProtocol, path: str):
    client = WsClient("unknown", websocket)
    await register(client)

    try:
        await listen(client)
    except websockets.ConnectionClosedOK as e:
        logger.info("Client closed the connection")
    finally:
        await unregister(client)


def start():
    logger.info("Starting server")
    start_server = websockets.serve(handle_connection, config.WS_HOST, config.WS_PORT)
    logger.info("Server started on {}:{}".format(config.WS_HOST, config.WS_PORT))

    asyncio.get_event_loop().run_until_complete(start_server)
    asyncio.get_event_loop().run_forever()


def close():
    logger.info("Closing websocket")
    for client in clients:
        logger.info("Unregistering client {}".format(client))

        client.last_disconnected = get_timestamp()
        db.update_client(client)

    clients.clear()
